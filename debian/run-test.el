(require 'package)
(require 'ert)

(unless (fboundp 'ert--skip-unless)
  (defun skip-unless (arg)
    (unless arg
      (setf (ert-test-expected-result-type
             (car ert--running-tests))
            :failed)
      (ert-fail (list nil)))))

(defun pdf-test-relative-edges-p (edges)
  (and (consp edges)
       (cl-every (lambda (x)
                   (and (numberp x)
                        (<= x 1)
                        (>= x 0)))
                 edges)))

(defmacro pdf-test-with-test-pdf (&rest body)
  (declare (indent 0) (debug t))
  (let ((buffer (make-symbol "buffer")))
    `(let ((,buffer (find-file-noselect
                     (expand-file-name "test.pdf"))))
       (pdf-info-quit)
       (pdf-info-process-assert-running t)
       (unwind-protect
           (with-current-buffer ,buffer ,@body)
         (when (buffer-live-p ,buffer)
           (set-buffer-modified-p nil)
           (kill-buffer))
         (pdf-info-quit)))))

(setq pdf-info-epdfinfo-program (expand-file-name "../server/epdfinfo"))

(dolist (file (directory-files "." t "\\.ert\\'"))
  (load-file file))

(package-initialize)
(require 'pdf-tools)

(ert-run-tests-batch-and-exit t)
